# dingapi

#### 介绍
各种类型的go语言api，dingapi是一些webapi，其余子包有相应的功能

#### 平台
支持windows(已测试)，linux（已测试）,mac（未测试）

#### 当前已收录
| 名称 | 用途          | 路径              |
|----|-------------|-----------------|
| 日志 | 输出日志，彩色（包含debug,info,error）     | dingapi/dinglog |
| 翻译 | 就是翻译嘛，支持各种语言(需联网) | Translate       |
|解方程|支持一元一次方程|Solve|
|Base64编码|如题|Base64Encode|
|Base64解码|如题|Base64Decode|
|Cmd调用模块|调用cmd通过各种方式|dingapi/dingcmd|
|百度aiApi调用模块|调用百度ai提供的功能|dingapi/dingbaidu|

### 用法大全 
[点我跳转](example.md)

### 提问

欢迎Issues，提出期待的功能

### 联系作者

QQ:1149978856 