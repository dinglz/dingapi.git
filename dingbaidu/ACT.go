package dingbaidu

import (
	"io/ioutil"
	"net/http"

	"github.com/buger/jsonparser"
)

func GetAccess_token(client_id string, client_secret string) string {
	res, _ := http.Get("https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=" + client_id + "&client_secret=" + client_secret)
	defer res.Body.Close()
	res_b, _ := ioutil.ReadAll(res.Body)
	res_s, _ := jsonparser.GetString(res_b, "access_token")
	return res_s
}
