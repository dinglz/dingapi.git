package dingbaidu

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/buger/jsonparser"
)

func Get_text_censor(text string, access_token string) (bool, string) {
	req := url.Values{"text": {text}}
	reqend := strings.NewReader(req.Encode())
	res, _ := http.Post("https://aip.baidubce.com/rest/2.0/solution/v1/text_censor/v2/user_defined?access_token="+access_token, "application/x-www-form-urlencoded", reqend)
	defer res.Body.Close()
	res_b, _ := ioutil.ReadAll(res.Body)
	//dinglog.Debug(string(res_b))
	conclusion, _ := jsonparser.GetString(res_b, "conclusion")
	if conclusion == "合规" {
		return true, ""
	}
	res_data := ""
	jsonparser.ArrayEach(res_b, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		cnt, _ := jsonparser.GetString(value, "msg")
		res_data += cnt
	}, "data")
	return false, res_data
}

func Get_img_censor(imgurl string, access_token string) (bool, string) {
	req := url.Values{"imgUrl": {imgurl}}
	reqend := strings.NewReader(req.Encode())
	res, _ := http.Post("https://aip.baidubce.com/rest/2.0/solution/v1/img_censor/v2/user_defined?access_token="+access_token, "application/x-www-form-urlencoded", reqend)
	defer res.Body.Close()
	res_b, _ := ioutil.ReadAll(res.Body)
	type_s, _ := jsonparser.GetString(res_b, "conclusion")
	if type_s == "合规" {
		return true, ""
	}
	res_data := ""
	jsonparser.ArrayEach(res_b, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		cnt, _ := jsonparser.GetString(value, "msg")
		res_data += cnt
	}, "data")
	return false, res_data
}
