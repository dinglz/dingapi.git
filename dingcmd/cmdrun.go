package dingcmd

import (
	"gitee.com/dinglz/dingapi/dinglog"
	"io/ioutil"
	"os/exec"
)

// Runcmd
//  @Description: 正常的访问方式
//  @param name
//  @param UseDinglogShowRes
//  @param args
//  @return string
func Runcmd(name string,UseDinglogShowRes bool,args ...string) string {
	cmd:=exec.Command(name,args...)
    out,_:=cmd.StdoutPipe()
	err:=cmd.Start()
	if err!=nil {
		if UseDinglogShowRes {
			dinglog.Error(err.Error())
			return err.Error()
		}
	}
	res,_:=ioutil.ReadAll(out)
	if UseDinglogShowRes {
		dinglog.Info(string(res))
	}
	return string(res)
}