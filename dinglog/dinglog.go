package dinglog

import (
	"path"
	"runtime"
	"strconv"
	"time"

	"github.com/gookit/color"
)

func maketime() string {
	timeres := time.Now()
	return timeres.Format("2006-01-02") + " " + strconv.Itoa(timeres.Hour()) + ":" + strconv.Itoa(timeres.Minute())
}

func Debug(text string) {
	_, file, line, _ := runtime.Caller(1)
	color.Yellowp("(/", path.Base(file), " ", line, ") ")
	color.Bold.Print(maketime(), " ")
	color.Print("[")
	color.Bluep(color.Bold.Text("debug"))
	color.Print("] ")
	color.Bold.Println(text)
}

func Info(text string) {
	color.Bold.Print(maketime(), " ")
	color.Print("[")
	color.Greenp(color.Bold.Text("info"))
	color.Print("] ")
	color.Bold.Println(text)
}

func Error(text string) {
	_, file, line, _ := runtime.Caller(1)
	color.Yellowp("(/", path.Base(file), " ", line, ") ")
	color.Bold.Print(maketime(), " ")
	color.Print("[")
	color.Redp(color.Bold.Text("erorr"))
	color.Print("] ")
	color.Bold.Println(color.Red.Text(text))
}
