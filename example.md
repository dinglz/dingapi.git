### 翻译


```go
dingapi.Translate("我爱你")//翻译
```
### dinglog日志

```go
dinglog.Info("我是info")
dinglog.Debug("我是debug")
dinglog.Error("我是error")
```

### Base64编码

```go
dingapi.Base64Encode("XXX") //base64编码
dingapi.Base64Decode("XXX") //base64解码
```

### dingcmd Cmd执行

```go
dingcmd.Runcmd("cmd.exe",true,"ipconfig")
```

### dingbaidu 百度aiApi调用

| 已支持           | 函数名          |
| ---------------- | --------------- |
| 获取Access_token | GetAccess_token |
| 图像审核         | Get_img_censor  |
| 文字审核         | Get_text_censor |

