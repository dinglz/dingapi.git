module gitee.com/dinglz/dingapi

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.1
	github.com/buger/jsonparser v1.1.1
	github.com/gookit/color v1.4.2
)
